Prometheus configuration
========================

Introduction
------------

Prometheus configuration to scrape data from all ghost
services.

Author
------

SOARES Lucas <lucas.soares@orange.com>

https://gitlab.com/ghostbutler/instrumentation/prometheus.git

